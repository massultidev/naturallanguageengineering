#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import json
import codecs

from utility.Resource import Resource

from botox.handlers.StatementHandlerBase import StatementHandlerBase

class BotOX(object):
    
    def __init__(self):
        with codecs.open(Resource.getResourcePath("init-corp.json"), "r", "utf-8") as f:
            self._initCorp = json.loads(f.read())["en"] #TODO: Adjust for other languages.
        self._handlers = []
        
        # Download basic NLTK corpus:
        nltkCorpNames = ["stopwords, wordnet", "punkt"]
        for name in nltkCorpNames:
            StatementHandlerBase.downloadCorpusNLTK(name)
    ##
    
    def addStatementHandler(self, hdClass, **kwargs):
        if not issubclass(hdClass, StatementHandlerBase):
            raise Exception("Every statement handler must be a derivative of '%s' class!" % StatementHandlerBase.__class__.__name__)
        self._handlers.append(hdClass(self._initCorp, **kwargs))
    ##
    
    def handleStatement(self, statement):
        if len(self._handlers) == 0:
            raise Exception("There are no statement handlers!")
        handleChances = []
        for handler in self._handlers:
            handleChances.append((handler.canHandle(unicode(statement)), handler))
        handleChances.sort(key=lambda lbd: lbd[0], reverse=True)
        return handleChances[0][1].handle(unicode(statement))
##
