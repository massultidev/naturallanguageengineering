#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import random

from botox.handlers.StatementHandlerBase import StatementHandlerBase
from botox.handlers.StatementHandlerRNN import StatementHandlerRNN

class StatementHandlerGreetings(StatementHandlerBase):
    
    class State(object):
        canBegin = 0
        canFinish = 1
        canResume = 2
    
    def __init__(self, initCorp, **kwargs):
        StatementHandlerBase.__init__(self, initCorp, **kwargs)
        
        self._state = None
        
        self._initCorpPrefix = "greetings"
    ##
    
    def canHandle(self, statement):
        isBegin = self._getChance(statement, "begin")
        isEnd = self._getChance(statement, "finish")
        
        if self._state is None:
            self._state = StatementHandlerGreetings.State.canBegin
        elif self._state == StatementHandlerGreetings.State.canBegin:
            self._state = StatementHandlerGreetings.State.canFinish
        
        if self._state == StatementHandlerGreetings.State.canBegin:
            return isBegin
        
        elif self._state == StatementHandlerGreetings.State.canFinish:
            return isEnd
        
        return 1.0
    ##
    
    def handle(self, statement):
        response = None
        
        if self._state == StatementHandlerGreetings.State.canBegin:
            response = random.choice(self._initCorp[self._initCorpPrefix]["begin"])
        
        elif self._state == StatementHandlerGreetings.State.canFinish:
            self._state = StatementHandlerGreetings.State.canResume
            response = random.choice(self._initCorp[self._initCorpPrefix]["finish"])
        
        elif self._state == StatementHandlerGreetings.State.canResume:
            self._state = StatementHandlerGreetings.State.canFinish
            response = random.choice(self._initCorp[self._initCorpPrefix]["resume"])
        
        return self._genEmoticon(self._genEndPunctuation(response))
    ##
    
    def _genEndPunctuation(self, statement):
        if statement.endswith(".") and random.getrandbits(1):
            statement = statement[:-1] + "!"
        return statement
    ##
    
    def _getChance(self, statement, typeName):
        return max([self._matchStatement(statement, model) for model in self._initCorp[self._initCorpPrefix][typeName]])
##
