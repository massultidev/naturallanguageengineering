#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import os
import nltk
import string
import random

class StatementHandlerBase(object):
    
    def __init__(self, initCorp, **kwargs):
        self._initCorp = initCorp
        
        for name, value in kwargs.items():
            setattr(self, name, value)
    ##
    
    def canHandle(self, statement):
        raise NotImplemented("This method must be overridden in subclasses!")
    ##
    
    def handle(self, statement):
        raise NotImplemented("This method must be overridden in subclasses!")
    ##
    
    def _genEmoticon(self, statement):
        if random.getrandbits(1):
            statement += u" " + random.choice(self._initCorp["emoticons"])
        return statement
    ##
    
    @classmethod
    def _matchStatement(cls, statement, model): # Jaccard similarity.
        if not isinstance(statement, list):
            statement = cls._tokenizeStatement(statement)
        if not isinstance(model, list):
            model = cls._tokenizeStatement(model)
        
        return len(set(statement).intersection(model))/float(len(set(statement).union(model)))
    ##
    
    @classmethod
    def _tokenizeStatement(cls, statement):
        stopwords = nltk.corpus.stopwords.words("english") #TODO: Adjust for other languages.
        stopwords.extend(string.punctuation)
        stopwords.append("")
        
        lemmatizer = nltk.stem.wordnet.WordNetLemmatizer() #TODO: Why I cannot choose language here?!
        
        tokenized = nltk.tokenize.word_tokenize(
            statement.lower().strip(string.punctuation),
            language="english" #TODO: Adjust for other languages.
        )
        tokenized = set(tokenized) - set(stopwords)
        
        return [lemmatizer.lemmatize(token) for token in tokenized]
    ##
    
    @staticmethod
    def downloadCorpusNLTK(name):
        dataPath = os.path.join(os.path.expanduser("~"), ".botox")
        if dataPath not in nltk.data.path:
            nltk.data.path.append(dataPath)
        try:
            nltk.data.find("%s.zip" % name)
        except LookupError:
            nltk.download(name, download_dir=dataPath)
##
