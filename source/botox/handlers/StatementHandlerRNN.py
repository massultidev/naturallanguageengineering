#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import re
import json
import numpy
import codecs
import string
import random

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils
from keras.models import model_from_json
import keras.backend as K

from botox.handlers.StatementHandlerBase import StatementHandlerBase

from utility.Resource import Resource

class StatementHandlerRNN(StatementHandlerBase):
    
    fileModel = "rnn-model.json"
    fileWeights = "rnn-weights.hdf5"
    fileDetails = "rnn-details.json"
    
    compileLoss = "categorical_crossentropy"
    compileOptimizer = "adam"
    
    detailsVocab = "vocab"
    detailsIntToChar = "int_to_char"
    detailsPatterns = "patterns"
    
    def __init__(self, initCorp, **kwargs):
        StatementHandlerBase.__init__(self, initCorp, **kwargs)
        
        self._model = None
        self._nVocab = None
        self._intToChar = None
        self._patterns = None
        
        self._sentenceTrials = 4
        self._sentenceLength = 30
        
        # Load the RNN model:
        K.set_learning_phase(1) # Dropout fix!
        with open(Resource.getResourcePath(self.fileModel), "r") as f:
            self._model = model_from_json(f.read())
        self._model.load_weights(Resource.getResourcePath(self.fileWeights))
        with codecs.open(Resource.getResourcePath(self.fileDetails), "r", "utf-8") as f:
            details = json.loads(f.read())
            self._nVocab = details[self.detailsVocab]
            self._intToChar = {int(key):val for key, val in details[self.detailsIntToChar].items()}
            self._patterns = details[self.detailsPatterns]
        self._model.compile(loss=self.compileLoss, optimizer=self.compileOptimizer)
    ##
    
    def canHandle(self, statement):
        return 0.25 # Always wants but does not dominate others.
    ##
    
    def handle(self, statement):
        generated = []
        
        for _ in range(self._sentenceTrials):
            pattern = self._patterns[numpy.random.randint(0, len(self._patterns)-1)] # Select initial pattern.
            initPatternLength = len(pattern) # Store initial pattern length.
            
            for _ in range(initPatternLength + self._sentenceLength):
                x = numpy.reshape(pattern, (1, len(pattern), 1))
                x = x / float(self._nVocab)
                prediction = self._model.predict(x, verbose=0)
                index = numpy.argmax(prediction)
                pattern.append(index)
                pattern = pattern[1:len(pattern)]
            
            sentence = ''.join([self._intToChar[value] for value in pattern])
            sentence = sentence.strip().split(" ", 1)
            if len(sentence) > 1:
                sentence = sentence[1]
            else:
                sentence = sentence[0]
            sentence = sentence.rsplit(" ", 1)[0]
            sentence = sentence[0].upper() + sentence[1:] + ("!" if random.getrandbits(1) else ".")
            generated.append((self._matchStatement(sentence, statement), sentence))
        
        # Return generated sentence that fits statement best:
        return self._genEmoticon(sorted(generated, key=lambda x: x[0], reverse=True)[0][1])
    ##
    
    @classmethod
    def createModel(cls, corpPath, lines=None, seqLength=None):
        if lines is None:
            lines = 10000
        if seqLength is None:
            seqLength = 100
        
        print("Loading file...")
        with codecs.open(corpPath, "r", "utf-8") as f:
            raw_text = "" #f.read()
            for _ in range(10000):
                raw_text += f.readline()
        
        print("Cleaning text...")
        raw_text = raw_text.lower()
        for token in string.punctuation:
            raw_text = raw_text.replace(token, "")
        re.sub("\s+", " ", raw_text)

        print("Creating patterns...")
        chars = sorted(list(set(raw_text)))
        charToInt = dict((c, i) for i, c in enumerate(chars))
        intToChar = dict((i, c) for i, c in enumerate(chars))
        
        # Summarize the loaded data:
        n_chars = len(raw_text)
        n_vocab = len(chars)
        print "Total Characters: ", n_chars
        print "Total Vocabulary: ", n_vocab
        
        # Prepare the dataset of input to output pairs encoded as integers:
        seq_length = 100
        patterns = []
        dataY = []
        for i in range(0, n_chars - seq_length, 1):
            seq_in = raw_text[i:i + seq_length]
            seq_out = raw_text[i + seq_length]
            patterns.append([charToInt[char] for char in seq_in])
            dataY.append(charToInt[seq_out])
        n_patterns = len(patterns)
        print "Total Patterns: ", n_patterns
        
        # Reshape X to be [samples, time steps, features]:
        X = numpy.reshape(patterns, (n_patterns, seq_length, 1))
        X = X / float(n_vocab)
        y = np_utils.to_categorical(dataY)
        
        # Define the LSTM model:
        print("Building model...")
        model = Sequential()
        model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2]), return_sequences=True))
        model.add(Dropout(0.2))
        model.add(LSTM(256))
        model.add(Dropout(0.2))
        model.add(Dense(y.shape[1], activation='softmax'))
        
        # Save details:
        with codecs.open(cls.fileDetails, "w", "utf-8") as f:
            f.write(json.dumps({
                cls.detailsVocab : n_vocab,
                cls.detailsIntToChar : intToChar,
                cls.detailsPatterns : patterns
            }))
        
        # Define the checkpoint:
        filepath="weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
        checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
        callbacks_list = [checkpoint]
        
        # Fit the model:
        print("Compiling model...")
        model.compile(loss=cls.compileLoss, optimizer=cls.compileOptimizer)
        print("Fitting model...")
        model.fit(X, y, nb_epoch=20, batch_size=128, verbose=1, callbacks=callbacks_list)
        
        # Save model:
        with open(cls.fileModel, "w") as f:
            f.write(model.to_json())
##
