#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import os
import sys

class Resource(object):
    
    _RPATH = "../data/R"
    
    def __init__(self):
        raise Exception("This class should not be instanced!")
    ##
    
    @classmethod
    def getResourcePath( cls, name ):
        return os.path.join(
            os.path.abspath( os.path.join( os.path.dirname( sys.modules["__main__"].__file__ ), cls._RPATH ) ),
            name
        )
##
