#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import wx

class PanelBottom(wx.Panel):
    
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        
        self.SetForegroundColour(wx.Colour(0, 0, 0))
        self.SetBackgroundColour(wx.Colour(255, 255, 255))
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        
        self._ctrlText = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_LEFT|wx.TE_MULTILINE|wx.TE_RICH|wx.TE_WORDWRAP)
        sizer.Add(self._ctrlText, 1, wx.EXPAND|wx.ALL, 5)
        
        self.SetSizer(sizer)
        self.Layout()
    ##
    
    def getText(self):
        return self._ctrlText.GetValue()
    def clear(self):
        self._ctrlText.SetValue(u"")
##
