#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import wx
import os
import sys
import Queue

from utility.Resource import Resource

from gui.PanelTop import PanelTop
from gui.PanelBottom import PanelBottom

from BotManager import BotManager

class Frame(wx.Frame):
    """This class is a representation of main window of 'BotOX' application.
    """
    def __init__(self):
        wx.Frame.__init__(self, parent=None, size=wx.Size(400, 300))
        
        TITLE = u"BotOX"
        BTN_SEND = u"Send"
        
        try:
            self.SetTitle( TITLE )
            self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
            self.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
            self.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )
            self.SetIcon( wx.Icon( Resource.getResourcePath( "appicon.png" ), wx.BITMAP_TYPE_PNG ) )
            
            sizer = wx.BoxSizer( wx.VERTICAL )
            
            self._splitter = wx.SplitterWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D )
            self._panelTop = PanelTop( self._splitter )
            self._panelBottom = PanelBottom( self._splitter )
            self._splitter.SplitHorizontally( self._panelTop, self._panelBottom )
            self._splitter.SetSashGravity( 0.7 )
            #self._splitter.Bind( wx.EVT_IDLE, self._splitterOnIdle )
            
            sizer.Add( self._splitter, 1, wx.EXPAND, 5 )
            
            self._btnSend = wx.Button( self, wx.ID_ANY, BTN_SEND, wx.DefaultPosition, wx.DefaultSize, 0 )
            sizer.Add( self._btnSend, 0, wx.ALIGN_RIGHT|wx.BOTTOM|wx.RIGHT|wx.LEFT, 5 )
            
            self._panelBottom.SetFocus()
            
            self.SetSizer(sizer)
            self.Layout()
            
            self.Centre(wx.BOTH)
            
            self.Show()
            
            # Bind events:
            self.Bind(wx.EVT_CLOSE, self._onExit)
            self.Bind(wx.EVT_THREAD, self._onThreadEvent)
            self.Bind(wx.EVT_MENU, self._onSend, id=wx.ID_OK)
            self._btnSend.Bind(wx.EVT_BUTTON, self._onSend)
            
            # Accelerator table:
            self.SetAcceleratorTable(wx.AcceleratorTable([
                (wx.ACCEL_CTRL, wx.WXK_RETURN, wx.ID_OK)
            ]))
            
            self._queue = Queue.Queue()
            self._botManager = BotManager(self, self._queue)
            self._botManager.start()
            
            self._isProcessing = False
            
        except KeyboardInterrupt:
            raise
        except Exception as e:
            wx.MessageBox(
                u"Type: '%s'\nWhat: '%s'\nClass: '%s'\nFile: '%s'\nLine: '%s'"
                %(unicode(type(e).__name__),
                unicode(str(e)),
                unicode(self.__class__.__name__),
                unicode(os.path.basename(__file__)).split(u".")[0],
                unicode(str(sys.exc_info()[2].tb_lineno))),
                u"Error",
                wx.OK|wx.ICON_ERROR
            )
            wx.GetApp().Exit()
    ##
    
    def _onExit(self, event):
        self._botManager.stop()
        wx.GetApp().Exit()
    ##
    
    def _onThreadEvent(self, event):
        response = unicode(event.GetString())
        self._panelTop.append(u"Bot", response)
        self._btnSend.Enable()
        self._isProcessing = False
    ##
    
    def _onSend(self, event):
        if self._isProcessing == True:
            return
        text = self._panelBottom.getText()
        if len(text) == 0:
            return
        self._isProcessing = True
        if self._btnSend.IsEnabled():
            self._btnSend.Disable()
        self._panelTop.append(u"Me", text)
        self._panelBottom.clear()
        self._queue.put(text)
##
