#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import wx
import os
import sys
import ctypes

from gui.Frame import Frame

def main():
    try:
        x11 = ctypes.cdll.LoadLibrary("libX11.so")
        x11.XInitThreads()
        
        app = wx.App()
        Frame()
        app.MainLoop()
        
    except KeyboardInterrupt:
        wx.GetApp().Exit()
    except Exception as e:
        wx.MessageBox(
            u"Type: '%s'\nWhat: '%s'\nFile: '%s'\nLine: '%s'"
            %(unicode(type(e).__name__),
            unicode(str(e)),
            unicode(os.path.basename(__file__)).split(u".")[0],
            str(sys.exc_info()[2].tb_lineno)),
            u"Error",
            wx.OK|wx.ICON_ERROR
        )
        wx.GetApp().Exit()
##

if __name__ == "__main__":
    main()
##
