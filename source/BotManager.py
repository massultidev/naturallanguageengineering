#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import wx
import os
import sys
import time
import threading
import traceback

from botox.BotOX import BotOX
from botox.handlers.StatementHandlerGreetings import StatementHandlerGreetings
from botox.handlers.StatementHandlerRNN import StatementHandlerRNN

class BotManager(threading.Thread):
    
    def __init__(self, parent, queue):
        threading.Thread.__init__(self)
        
        self._parent = parent
        self._event = threading.Event()
        self._queue = queue
        
        self._bot = None
    ##
    
    def stop(self):
        self._event.set()
    ##
    
    def run(self):
        try:
            self._bot = BotOX()
            self._bot.addStatementHandler(StatementHandlerGreetings)
            self._bot.addStatementHandler(StatementHandlerRNN)
            
            while not self._event.isSet():
                try:
                    time.sleep(0.5)
                    
                    if not self._queue.empty():
                        statement = self._queue.get()
                        response = self._bot.handleStatement(statement)
                        
                        event = wx.ThreadEvent()
                        event.SetString(unicode(str(response)))
                        self._parent.GetEventHandler().QueueEvent(event.Clone())
                    
                except:
                    traceback.print_exc()
            
            self._event.clear()
            
        except KeyboardInterrupt:
            raise
        except Exception as e:
            traceback.print_exc()
            wx.MessageBox(
                u"Type: '%s'\nWhat: '%s'\nClass: '%s'\nFile: '%s'\nLine: '%s'"
                %(unicode(type(e).__name__),
                unicode(str(e)),
                unicode(self.__class__.__name__),
                unicode(os.path.basename(__file__)).split(u".")[0],
                unicode(str(sys.exc_info()[2].tb_lineno))),
                u"Error",
                wx.OK|wx.ICON_ERROR
            )
            wx.GetApp().Exit()
##
